﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris_by_Aleks_Fedyczek.Viev
{
	public interface IViev
	{
		event Action StartNewGame;
		event Action Pause;
		event Action Resume;
		event Func<bool[,]> GetBoard;
		event Func<int> UpdateScore;

		void UpdateScoreBox();

		event Action MoveRight;
		event Action MoveLeft;
		event Action MoveDown;
		event Action Rotate;


	}
}
