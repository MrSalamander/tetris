﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Tetris_by_Aleks_Fedyczek.Viev
{

	public partial class Viev : Form, IViev
	{
		
		#region Properties
		public bool[,] Board { get; set; }
		public int Score
		{
			get
			{
				return Score;
			}
			set
			{
				
			}
		}
		#endregion

		#region Ctor
		public Viev()
		{
			InitializeComponent();
			graphics = Board_Panel.CreateGraphics();
			pen = new Pen(Color.FromArgb(53, 68, 99));
			brush = new SolidBrush(Color.Red);

			_timer = new Timer();
			_timer.Interval = 1000;
			_timer.Start();
			_timer.Tick += RefreshBoard;

		}
		#endregion

		#region Events
		public event Action StartNewGame;
		public event Func<bool[,]> GetBoard;
		public event Action Pause;
		public event Action Resume;
		public event Action MoveRight;
		public event Action MoveLeft;
		public event Action MoveDown;
		public event Func<int> UpdateScore;
		public event Action Rotate;
		#endregion

		private void DrawLines()
		{
			for (int x = 0; x <= BOARD_WIDTH; x++)
			{
				graphics.DrawLine(pen, x * BOARD_SCALE, 0 * BOARD_SCALE, x* BOARD_SCALE, BOARD_SCALE * BOARD_HEIGHT);
			}
			for (int y = 0; y < BOARD_HEIGHT; y++)
			{
				graphics.DrawLine(pen, 0 * BOARD_SCALE, y * BOARD_SCALE, BOARD_WIDTH * BOARD_SCALE, y * BOARD_SCALE);
			}
		}

		private void RefreshBoard(object sender, EventArgs e)
		{
			try
			{
				
				Board = GetBoard?.Invoke();
				Board_Panel.Refresh();
				for (int x = 0; x < BOARD_WIDTH; x++)
				{
					for (int y = 0; y < BOARD_HEIGHT; y++)
					{
						if(Board[x,y])
						{
							graphics.FillRectangle(brush, x * BOARD_SCALE +1, y * BOARD_SCALE + 1, BOARD_SCALE-2, BOARD_SCALE-2);
						}
					}
				}
				//DrawLines();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		private void NewGame_Button_Click(object sender, EventArgs e)
		{
			StartNewGame?.Invoke();
			RefreshBoard(sender, e);
		}

		private void Pause_Button_Click(object sender, EventArgs e)
		{
			Pause?.Invoke();
			_timer.Stop();
		}

		private void Resume_Button_Click(object sender, EventArgs e)
		{
			Resume?.Invoke();
			_timer.Start();
		}

		private void Board_Panel_Paint(object sender, PaintEventArgs e)
		{
			//DrawLines();
		}

		void IViev.UpdateScoreBox()
		{
			Score_Box.Text = UpdateScore?.Invoke().ToString();
		}

		#region Fields
		private Timer _timer;
		private const int BOARD_SCALE = 35;
		private const uint BOARD_WIDTH = 12;
		private const uint BOARD_HEIGHT = 20;
		private Graphics graphics;
		private SolidBrush brush;
		private Pen pen;
		private int score;

		#endregion


		//private void Board_Panel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		//{
		//	switch (e.KeyCode)
		//	{
		//		case Keys.W:
		//			e.IsInputKey = true;
		//			Console.WriteLine("lewy");
		//			break;
		//		case Keys.Right:
		//			e.IsInputKey = true;
		//			break;
		//	}

		//}

		private void Viev_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.A:
					MoveLeft?.Invoke();
					RefreshBoard(sender, e);
					break;
				case Keys.D:
					MoveRight?.Invoke();
					RefreshBoard(sender, e);
					break;
				case Keys.S:
					MoveDown?.Invoke();
					RefreshBoard(sender,e);
					break;
				case Keys.W:
					Rotate?.Invoke();
					RefreshBoard(sender,e);
					break;
			}
		}
	}

}
