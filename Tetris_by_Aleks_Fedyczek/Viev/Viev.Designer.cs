﻿namespace Tetris_by_Aleks_Fedyczek.Viev
{
	partial class Viev
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Board_Panel = new System.Windows.Forms.Panel();
			this.Author_Label = new System.Windows.Forms.Label();
			this.Score_Box = new System.Windows.Forms.TextBox();
			this.NewGame_Button = new System.Windows.Forms.Button();
			this.Pause_Button = new System.Windows.Forms.Button();
			this.Resume_Button = new System.Windows.Forms.Button();
			this.NextFigure_Panel = new System.Windows.Forms.Panel();
			this.NextFiguer_Label = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Board_Panel
			// 
			this.Board_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
			this.Board_Panel.Location = new System.Drawing.Point(13, 13);
			this.Board_Panel.Name = "Board_Panel";
			this.Board_Panel.Size = new System.Drawing.Size(420, 700);
			this.Board_Panel.TabIndex = 0;
			this.Board_Panel.Paint += new System.Windows.Forms.PaintEventHandler(this.Board_Panel_Paint);
		
			// 
			// Author_Label
			// 
			this.Author_Label.AutoSize = true;
			this.Author_Label.ForeColor = System.Drawing.Color.Gainsboro;
			this.Author_Label.Location = new System.Drawing.Point(450, 28);
			this.Author_Label.Name = "Author_Label";
			this.Author_Label.Size = new System.Drawing.Size(144, 15);
			this.Author_Label.TabIndex = 1;
			this.Author_Label.Text = "Tetris_by_Alex_Fedyczek";
			// 
			// Score_Box
			// 
			this.Score_Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.Score_Box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Score_Box.Enabled = false;
			this.Score_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.Score_Box.ForeColor = System.Drawing.Color.LightGray;
			this.Score_Box.HideSelection = false;
			this.Score_Box.Location = new System.Drawing.Point(453, 77);
			this.Score_Box.Name = "Score_Box";
			this.Score_Box.ReadOnly = true;
			this.Score_Box.Size = new System.Drawing.Size(154, 36);
			this.Score_Box.TabIndex = 2;
			this.Score_Box.Text = "SCORE";
			// 
			// NewGame_Button
			// 
			this.NewGame_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
			this.NewGame_Button.FlatAppearance.BorderColor = System.Drawing.Color.Red;
			this.NewGame_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.NewGame_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.NewGame_Button.ForeColor = System.Drawing.Color.LightGray;
			this.NewGame_Button.Location = new System.Drawing.Point(453, 438);
			this.NewGame_Button.Name = "NewGame_Button";
			this.NewGame_Button.Size = new System.Drawing.Size(154, 80);
			this.NewGame_Button.TabIndex = 3;
			this.NewGame_Button.Text = "NEW GAME";
			this.NewGame_Button.UseVisualStyleBackColor = false;
			this.NewGame_Button.Click += new System.EventHandler(this.NewGame_Button_Click);
			// 
			// Pause_Button
			// 
			this.Pause_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
			this.Pause_Button.FlatAppearance.BorderColor = System.Drawing.Color.Red;
			this.Pause_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.Pause_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.Pause_Button.ForeColor = System.Drawing.Color.LightGray;
			this.Pause_Button.Location = new System.Drawing.Point(453, 536);
			this.Pause_Button.Name = "Pause_Button";
			this.Pause_Button.Size = new System.Drawing.Size(154, 80);
			this.Pause_Button.TabIndex = 4;
			this.Pause_Button.Text = "PAUSE";
			this.Pause_Button.UseVisualStyleBackColor = false;
			this.Pause_Button.Click += new System.EventHandler(this.Pause_Button_Click);
			// 
			// Resume_Button
			// 
			this.Resume_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
			this.Resume_Button.FlatAppearance.BorderColor = System.Drawing.Color.Red;
			this.Resume_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.Resume_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.Resume_Button.ForeColor = System.Drawing.Color.LightGray;
			this.Resume_Button.Location = new System.Drawing.Point(453, 633);
			this.Resume_Button.Name = "Resume_Button";
			this.Resume_Button.Size = new System.Drawing.Size(154, 80);
			this.Resume_Button.TabIndex = 5;
			this.Resume_Button.Text = "RESUME";
			this.Resume_Button.UseVisualStyleBackColor = false;
			this.Resume_Button.Click += new System.EventHandler(this.Resume_Button_Click);
			// 
			// NextFigure_Panel
			// 
			this.NextFigure_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
			this.NextFigure_Panel.Location = new System.Drawing.Point(453, 266);
			this.NextFigure_Panel.Name = "NextFigure_Panel";
			this.NextFigure_Panel.Size = new System.Drawing.Size(154, 154);
			this.NextFigure_Panel.TabIndex = 1;
			// 
			// NextFiguer_Label
			// 
			this.NextFiguer_Label.AutoSize = true;
			this.NextFiguer_Label.ForeColor = System.Drawing.Color.Gainsboro;
			this.NextFiguer_Label.Location = new System.Drawing.Point(487, 248);
			this.NextFiguer_Label.Name = "NextFiguer_Label";
			this.NextFiguer_Label.Size = new System.Drawing.Size(87, 15);
			this.NextFiguer_Label.TabIndex = 6;
			this.NextFiguer_Label.Text = "NEXT FIGURE";
			// 
			// Viev
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(625, 726);
			this.Controls.Add(this.NextFiguer_Label);
			this.Controls.Add(this.NextFigure_Panel);
			this.Controls.Add(this.Resume_Button);
			this.Controls.Add(this.Pause_Button);
			this.Controls.Add(this.NewGame_Button);
			this.Controls.Add(this.Score_Box);
			this.Controls.Add(this.Author_Label);
			this.Controls.Add(this.Board_Panel);
			this.KeyPreview = true;
			this.Name = "Viev";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Viev_KeyDown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel Board_Panel;
		private System.Windows.Forms.Label Author_Label;
		private System.Windows.Forms.TextBox Score_Box;
		private System.Windows.Forms.Button NewGame_Button;
		private System.Windows.Forms.Button Pause_Button;
		private System.Windows.Forms.Button Resume_Button;
		private System.Windows.Forms.Panel NextFigure_Panel;
		private System.Windows.Forms.Label NextFiguer_Label;
	}
}