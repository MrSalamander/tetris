﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris_by_Aleks_Fedyczek.Model
{
	public class Figure
	{
		public enum Types
		{
			I_block,
			J_block,
			L_block,
			O_block,
			S_block,
			T_block,
		}
		public Color Color { get; set; }
		public int LeftCorner_X_Position { get; set; }
		public int LeftCorner_Y_Position { get; set; }

		public Types Type { get; set; }
		public bool[,] Blocks { get; set; }

		public void Turn()
		{
			int N = 4;
			//bool[,] temp = new bool[4, 4];
			for (int x = 0; x < N / 2; x++)
			{
				// Consider elements in group of 4 in  
				// current square 
				for (int y = x; y < N - x - 1; y++)
				{
					// store current cell in temp variable 
					bool temp = Blocks[x,y];

					// move values from right to top 
					Blocks[x, y] = Blocks[y, (N - 1 - x)];

					// move values from bottom to right 
					Blocks[y, (N - 1 - x)] = Blocks[(N - 1 - x),(N - 1 - y)];

					// move values from left to bottom 
					Blocks[(N - 1 - x),(N - 1 - y)] = Blocks[(N - 1 - y),x];

					// assign temp to left 
					Blocks[(N - 1 - y),x] = temp;
				}
			}
		}


		#region Ctor
		public Figure()
		{

			Type = RandomEnumValue<Types>();
			CreateFigure();
			
		}
		
		private void CreateFigure()
		{
			switch (Type)
			{
				case Types.I_block:
					Create_I_block();
					break;
				case Types.J_block:
					Create_J_block();
					break;
				case Types.L_block:
					Create_L_block();
					break;
				case Types.O_block:
					Create_O_block();
					break;
				case Types.S_block:
					Create_S_block();
					break;
				case Types.T_block:
					Create_T_block();
					break;
			}
		}

		private void Create_T_block()
		{
			ClearBlocks();
			Color = Color.AliceBlue;
			Blocks[1, 0] = true;
			Blocks[1, 1] = true;
			Blocks[1, 2] = true;
			Blocks[2, 1] = true;
		}

		private void Create_S_block()
		{
			ClearBlocks();
			Color = Color.Red;
			Blocks[1, 0] = true;
			Blocks[1, 1] = true;
			Blocks[2, 1] = true;
			Blocks[2, 2] = true;
		}

		private void Create_O_block()
		{
			ClearBlocks();
			Color = Color.Yellow;
			Blocks[1, 0] = true;
			Blocks[1, 1] = true;
			Blocks[2, 0] = true;
			Blocks[2, 1] = true;
		}

		private void Create_L_block()
		{
			ClearBlocks();
			Color = Color.Green;
			Blocks[1, 0] = true;
			Blocks[1, 1] = true;
			Blocks[1, 2] = true;
			Blocks[2, 2] = true;
		}

		private void Create_J_block()
		{
			ClearBlocks();
			Color = Color.LemonChiffon;
			Blocks[2, 0] = true;
			Blocks[2, 1] = true;
			Blocks[2, 2] = true;
			Blocks[1, 2] = true;
		}

		private void Create_I_block()
		{
			ClearBlocks();
			Color = Color.Orange;
			Blocks[1, 0] = true;
			Blocks[1, 1] = true;
			Blocks[1, 2] = true;
			Blocks[1, 3] = true;
		}

	

		#endregion

		public int LowestLevelInFigure()
		{
			for (int y = 3; y >= 0; y--) 
			{
				for (int x = 0; x < 4; x++)
				{
					if (Blocks[x, y]) return y;
				}
			}
			return 0;
		}

		public int LeftFarestBlockInFigure()
		{
			for (int x = 0; x < 4; x++) 
			{
				for (int y = 0; y < 4; y++)
				{
					if (Blocks[x, y]) return x;
				}
			}
			return 3;
		}

		public int RightFarestBlockInFigure()
		{
			for (int x = 3; x >= 0; x--)
			{
				for (int y = 3; y >= 0; y--)
				{
					if (Blocks[x, y]) return x;
				}
			}
			return 0;
		}

		private void ClearBlocks()
		{
			Blocks = new bool[4, 4];
			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					Blocks[x, y] = false;
				}
			}
		}



		#region Fields
		private bool[,] _blocks = new bool[4, 4];
		
		#endregion


		#region CustonGenericType
		static T RandomEnumValue<T>()
		{
			var v = Enum.GetValues(typeof(T));
			return (T)v.GetValue(new Random().Next(v.Length));
		}
		#endregion
	}
}
