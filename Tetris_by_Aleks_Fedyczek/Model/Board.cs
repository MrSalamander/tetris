﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris_by_Aleks_Fedyczek.Model
{
	public class Board
	{
		public bool[,] IsFilled { get; set; }






		#region Ctor
		public Board(uint _dimensionX, uint _dimensionY)
		{
			// Create board
			this._dimensionX = _dimensionX;
			this._dimensionY = _dimensionY;
			IsFilled = new bool[_dimensionX,_dimensionY+1];
			ClearBoard();

		}
		#endregion

		

		public void ClearBoard()
		{
			IsFilled = new bool[_dimensionX, _dimensionY + 1];
			for (int x = 0; x<_dimensionX; x++)
			{
				for (int y = 0; y < _dimensionY; y++)
				{
					IsFilled[x,y] = false;
				}
			}

			for (int x = 0; x < _dimensionX; x++)
			{
				IsFilled[x, _dimensionY] = true;
			}
			}

		public bool CheckIfLineIsFilled(uint _yParam)
		{
			for(int x = 0; x<_dimensionX; x++)
			{
				if (!IsFilled[x, _yParam])
					return false;
			}
			return true;
		}

		public readonly uint _dimensionX;
		public readonly uint _dimensionY;


	}
}
