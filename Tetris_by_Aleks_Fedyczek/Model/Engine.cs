﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris_by_Aleks_Fedyczek.Model
{
	public class Engine
	{
		const uint DIMENSION_WIDTH = 12;
		const uint DIMENSION_HEIGHT = 20;

		#region Properties
		public Board Board { get; set; }
		public Figure Figure { get; set; }
		public int Score { get; set; }

		#endregion

		public event Action UpdateScoreInViev;

		#region Ctor
		public Engine()
		{
			Board = new Board(DIMENSION_WIDTH, DIMENSION_HEIGHT);
			//
			// _timer
			//
			_timer = new Timer();
			_timer.Interval = 1000;
		
			_timer.Tick += FallDownTick;
		}
		#endregion

		#region Methods
		public void StartNewGame()
		{
			Board = new Board(DIMENSION_WIDTH, DIMENSION_HEIGHT);
			Score = 0;
			GenerateNewFigure();
			_timer.Start();

		}

		public void Pause()
		{
			_timer.Stop();
		}

		public void Resume()
		{
			_timer.Start();
		}

		public bool[,] ReturnBoard()
		{
			return Board.IsFilled;
		}

		public void MoveLeft()
		{
			if(CanGoLeft())
			{ 
				ClearPreviousFigurePosition();
				Figure.LeftCorner_X_Position--;
				DrawFigureAtPosition();
			}
		}

		public void MoveRight()
		{
			if (CanGoRight())
			{
				ClearPreviousFigurePosition();
				Figure.LeftCorner_X_Position++;
				DrawFigureAtPosition();
			}
		}

		public void MoveDown()
		{
			if (CanGoDown())
			{
				ClearPreviousFigurePosition();
				Figure.LeftCorner_Y_Position++;
				DrawFigureAtPosition();
			}
			else
			{
				GenerateNewFigure();
			}
		}

		public void Rotate()
		{
			ClearPreviousFigurePosition();
			Figure.Turn();
			DrawFigureAtPosition();
		}



		private void GenerateNewFigure()
		{
			CheckIfLineIsFull();
			Figure = new Figure();
			Figure.LeftCorner_X_Position = 4;
			Figure.LeftCorner_Y_Position = -3;
			// spawn only last line from figure, rest is over board
			Board.IsFilled[4, 0] = Figure.Blocks[0, 3];
			Board.IsFilled[5, 0] = Figure.Blocks[1, 3];
			Board.IsFilled[6, 0] = Figure.Blocks[2, 3];
			Board.IsFilled[7, 0] = Figure.Blocks[3, 3];
		}

		private void FallDownTick(object sender, EventArgs e)
		{
			MoveDown();
		}

		private void DrawFigureAtPosition()
		{
			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					if (Figure.Blocks[x, y])
					{
						if (x + Figure.LeftCorner_X_Position >= 0 && y + Figure.LeftCorner_Y_Position >= 0)
							Board.IsFilled[x + Figure.LeftCorner_X_Position, y + Figure.LeftCorner_Y_Position] = true;
					}
				}
			}
		}

		private void ClearPreviousFigurePosition()
		{
			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					if (Figure.Blocks[x, y])
					{
						if (x + Figure.LeftCorner_X_Position >= 0 && y + Figure.LeftCorner_Y_Position >= 0)
							Board.IsFilled[x + Figure.LeftCorner_X_Position, y + Figure.LeftCorner_Y_Position] = false;
					}
				}
			}
		}

		private bool CanGoLeft()
		{
			if ((Figure.LeftCorner_X_Position + Figure.LeftFarestBlockInFigure()) > 0) return true;
			else return false;
		}

		private bool CanGoRight()
		{
			if ((Figure.LeftCorner_X_Position + Figure.RightFarestBlockInFigure()) < DIMENSION_WIDTH-1) return true;
			else return false;
		}

		private bool CanGoDown()
		{
			for (int x = Figure.LeftFarestBlockInFigure(); x <= Figure.RightFarestBlockInFigure(); x++)
			{
				if (Figure.LeftCorner_Y_Position < 0)
					return true; //initialize positio
				if (Board.IsFilled[ (x + Figure.LeftCorner_X_Position) , Figure.LeftCorner_Y_Position + Figure.LowestLevelInFigure()] 
					&& 
					Board.IsFilled[ (x + Figure.LeftCorner_X_Position) , (Figure.LeftCorner_Y_Position + 1 + Figure.LowestLevelInFigure())])
				{
					return false;
				}
			}
			return true;
		}

		private bool CheckLine(int y)
		{
			for(int x = 0; x < DIMENSION_WIDTH; x++)
			{
				if (!Board.IsFilled[x, y])
					return false;
			}
			return true;
		}

		private void PullDownAllAbove(int fromY)
		{
			for(int x = 0; x < DIMENSION_WIDTH; x++)
			{
				for(int y = fromY; y>0; y--)
				{
					Board.IsFilled[x, y] = Board.IsFilled[x, y - 1];
				}
			}
			UpdateScoreInViev?.Invoke();
		}

		public int UpdateScore()
		{
			return Score;
		}

		private void CheckIfLineIsFull()
		{
			for (int y = 0; y < DIMENSION_HEIGHT; y++)
			{
				if (CheckLine(y))
				{
					Score += 12;

					PullDownAllAbove(y);
				}
			}
		}
		#endregion


		#region Fields
		private Timer _timer;

		#endregion
	}
}
