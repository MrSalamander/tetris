﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris_by_Aleks_Fedyczek.Presenter
{
	public class Presenter
	{
		private Model.Engine _engine;
		private Viev.IViev _viev;

		public Presenter(Model.Engine _engine, Viev.IViev _viev)
		{
			this._engine = _engine;
			this._viev = _viev;
			ObserveViev();
			ObserveModel();

		}

		private void ObserveViev()
		{
			_viev.GetBoard += _engine.ReturnBoard;
			_viev.StartNewGame += _engine.StartNewGame;
			_viev.Pause += _engine.Pause;
			_viev.Resume += _engine.Resume;
			_viev.MoveLeft += _engine.MoveLeft;
			_viev.MoveRight += _engine.MoveRight;
			_viev.MoveDown += _engine.MoveDown;
			_viev.UpdateScore += _engine.UpdateScore;
			_viev.Rotate += _engine.Rotate;
		}

		private void ObserveModel()
		{
			_engine.UpdateScoreInViev += _viev.UpdateScoreBox;
		}
	

	}
}
