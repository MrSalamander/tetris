﻿using System;
using System.Windows.Forms;

namespace Tetris_by_Aleks_Fedyczek
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Model.Engine _engine = new Model.Engine();
			Viev.Viev _viev = new Viev.Viev();
			Presenter.Presenter _presenter = new Presenter.Presenter(_engine, _viev);

			Application.Run(_viev);
		}
	}
}
